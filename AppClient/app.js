'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
    .module('StoreLocator', [
        'StoreLocator.controllers',
        'ui.router',
        'StoreLocator.services'
    ])
    .config(['$stateProvider','$urlRouterProvider',function ($stateProvider,$urlRouterProvider) {


        $urlRouterProvider.otherwise('/NearestStore');

        $stateProvider

            .state('store',{
                templateUrl:'views/getStores.html',
                url:'/NearestStore',
                controller:'GetNearestStore'

            })

    }]);


