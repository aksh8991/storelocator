module.exports = function (server, db) {


    server.get("/api/getNeraestStore", function (req, res, next) {

        var item = req.params;
            db.stores.findOne({ loc : { $near :  [parseFloat(item.lng), parseFloat(item.lat)]}

            },function (err, list) {
                var list1 = [];



                    var longitudeOfNearestStore = list.loc[0];
                    var longitudeOfSourceEntered = item.lng;
                    var latitudeofSourceEntered = item.lat;
                    var latitudeofNearestStore = list.loc[1];

                    var R = 6371; // Radius of the earth in km
                    var dLat = (latitudeofSourceEntered - latitudeofNearestStore) * (Math.PI / 180);  // deg2rad below
                    var dLon = (longitudeOfNearestStore - longitudeOfSourceEntered) * (Math.PI / 180);
                    var a =
                            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                            Math.cos((latitudeofNearestStore) * (Math.PI / 180)) * Math.cos((latitudeofSourceEntered) * (Math.PI / 180)) *
                            Math.sin(dLon / 2) * Math.sin(dLon / 2)
                        ;
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = R * c; // Distance in km




                list1.push(list);
                list1.push({dist:d});



                res.writeHead(200, {
                    'Content-Type': 'application/json; charset=utf-8'

                });
                res.end(JSON.stringify(list1))
            });
        });


}